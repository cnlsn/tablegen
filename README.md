# tablegen
A number conversion table generator.

This application asks the user to input the base they wish to convert from and to, and the start and finish points of the table in base 10.  Once submitted, `generate.php` will then generate the table, using the `base_convert()` function on each iteration to convert the current value of the variable `$i` to its equivelant values in the other two bases (`$b1` and `$b2`).  This occurs on each iteration, until the table has populated values for the range specified.
