<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Table Generator</title>
    </head>
    <body>
        <h1>Conversion Table Generator</h1>
        <h2>Written by Connor Nielsen</h2>
        <p>This will generate a number conversion table for converting between two bases.<br />
        To start, select the bases you are converting from and to, then enter the minimum and maximum values for the table (in decimal).</p>
        <form action="generate.php" method="post">
            <fieldset>
                <label for="selFirstBase">Select first base:
                    <select id="selFirstBase" name="selFirstBase">
                        <option value="2">Binary</option>
                        <option value="8">Octal</option>
                        <option value="10">Decimal</option>
                        <option value="16">Hexadecimal</option>
                    </select>
                </label>
                <label for="selSecondBase">Select second base:
                    <select id="selSecondBase" name="selSecondBase">
                        <option value="2">Binary</option>
                        <option value="8">Octal</option>
                        <option value="10">Decimal</option>
                        <option value="16">Hexadecimal</option>
                    </select>
                </label>
                <label for="firstNum">Table floor:
                    <input type="text" name="firstNum" id="firstNum" />
                </label>
                <label for="secondNum">Table ceiling:
                    <input type="text" name="secondNum" id="secondNum" />
                </label>
            </fieldset>
            <input type="submit" value="submit" />
        </form>
    </body>
</html>