<?php
    $b1 = $_POST['selFirstBase'];
    $b2 = $_POST['selSecondBase'];
    $num1 = $_POST['firstNum'];
    $num2 = $_POST['secondNum'];
        
    echo "<table>
            <thead>
            <tr>
                <th>Dec</th>
                <th>".nameBase($b1)."</th>
                <th>".nameBase($b2)."</th>
            </tr>
            </thead>
            <tbody>";        
    for($i = $num1; $i <= $num2; $i = $i + 1) {
        echo "<tr>
                <td>".$i."</td>
                <td>".base_convert($i,10,$b1)."</td>
                <td>".base_convert($i,10,$b2)."</td>
            </tr>";
    }
    echo "</tbody></table>";
    
    function nameBase($b) {
        if($b == 2) {
            return "Bin";   
        } else if($b == 8) {
            return "Oct";
        } else if($b == 10) {
            return "Dec";
        } else if($b == 16) {
            return "Hex";
        } else {
            return "Unknown";   
        }
    }
?>